package com.cabscustomer.transformers;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cabscustomer.R;

/**
 * Created by Manoj sadhu on 8/6/2017.
 */

public class CustomTransformer implements ViewPager.PageTransformer {
    private float minAlpha;
    private int degrees;
    private float distanceToCentreFactor;
    private ImageView phoneImg;
    private TextView title;
    private TextView description;

    public void transformPage(View view, float position) {
        phoneImg = (ImageView) view.findViewById(R.id.phoneImg);
        title = (TextView) view.findViewById(R.id.title);
        description = (TextView) view.findViewById(R.id.description);
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();
        view.setPivotX((float) pageWidth / 2);
        view.setPivotY((float) (pageHeight + pageWidth * distanceToCentreFactor));

        if (position < -1) { //[-infinity,1)
            //off to the left by a lot
            view.setRotation(0);
            view.setAlpha(0);
        } else if (position <= 1) { //[-1,1]
            //  view.setTranslationX((-position) * pageWidth); //shift the view over
            view.setRotation(position * (13)); //rotate it
            // Fade the page relative to its distance from the center
            view.setAlpha(Math.max(minAlpha, 1 - Math   .abs(position) / 3));

            //phoneImg.setTranslationX((float) (-(position) * 0.3 * pageWidth));
            title.setTranslationX((float) ((position) * 0.4 * pageWidth));
            description.setTranslationX((float) ((position) * 1.2 * pageWidth));

        } else { //(1, +infinity]
            //off to the right by a lot
            view.setRotation(0);
            view.setAlpha(0);
        }
    }
}