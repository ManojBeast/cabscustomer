package com.cabscustomer.customViews;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewAnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;

import com.cabscustomer.R;

/**
 * Created by Manoj sadhu on 9/3/2017.
 */

public class EditTextAnimation extends android.support.v7.widget.AppCompatEditText {
Paint paint;
    public EditTextAnimation(Context context) {
        super(context);
        init();
    }

    public EditTextAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init(){
        setBackgroundResource(R.drawable.edittext_bottom_line);
        paint = new Paint();
    }


    @Override
    public void onDraw(Canvas canvas){

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);
        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN && !isFocused()) {
            int centreX= (int) (this.getX() + this.getWidth()  / 2);
            int centreY= (int) (this.getY() + this.getHeight() / 2);
            Animator animator = ViewAnimationUtils.createCircularReveal(this,
                    centreX,
                    centreY,
                    0,
                    this.getHeight()*4).setDuration(300);
            animator.setInterpolator(new LinearInterpolator());
            animator.start();
        }

        return super.onTouchEvent(event);
    }



}
