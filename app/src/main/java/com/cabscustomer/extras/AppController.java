package com.cabscustomer.extras;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.cabscustomer.dabase.UserDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;


/**
 * Created by Manoj sadhu on 8/26/2017.
 */

public class AppController extends Application {
    public static String baseUrl = "http://hotfindzdev.com/";
    public static final String TAG = "VolleyPatterns";
    private RequestQueue mRequestQueue;
    private static AppController sInstance;
    public static ProgressDialog progress;
    public static SQLiteDatabase userDetailsSqlDB;
    public static UserDetails userDetailsdb;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        userDetailsdb = new UserDetails(this);

    }
    public static synchronized AppController getInstance() {
        return sInstance;
    }
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            CookieManager cookieManager = new CookieManager(new PersistentCookieStore(getApplicationContext()), CookiePolicy.ACCEPT_ORIGINAL_SERVER);
            CookieHandler.setDefault(cookieManager);
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        VolleyLog.e("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);
    }
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);

        getRequestQueue().add(req);
    }
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:

        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


    public static void showProgres(final Context context) {
        progress = new ProgressDialog(context);
        progress.setMessage("Loading Data.Please Wait ...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);
        progress.setProgress(0);
        progress.setProgressNumberFormat(null);
        progress.setProgressPercentFormat(null);
        progress.setCancelable(false);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
    }

    public static void stopProgres() {
        progress.dismiss();
    }

    public static String[] getuserDeatils() {
        String[] userDetails = new String[5];
        userDetailsSqlDB = userDetailsdb.getReadableDatabase();
        Cursor cursor = userDetailsSqlDB.rawQuery("SELECT * FROM user", null);
        if (cursor != null && cursor.moveToFirst()) {
            userDetails[0] = cursor.getString(cursor.getColumnIndex("FirstName"));
            userDetails[1] = cursor.getString(cursor.getColumnIndex("LastName"));
            userDetails[2] = cursor.getString(cursor.getColumnIndex("Email"));
            userDetails[3] = cursor.getString(cursor.getColumnIndex("MobileNumber"));
            userDetails[4] = cursor.getString(cursor.getColumnIndex("CountryCode"));
            cursor.close();
            userDetailsdb.close();
            return userDetails;
        } else {
            return userDetails;
        }
    }

    public static void insertUserDetails(JSONObject userDetailsJson) throws JSONException {
        String FirstName = userDetailsJson.getString("FirstName");
        String LastName = userDetailsJson.getString("LastName");
        String Email = userDetailsJson.getString("Email");
        String MobileNumber = userDetailsJson.getString("MobileNumber");
        String CountryCode = userDetailsJson.getString("CountryCode");

        userDetailsSqlDB = userDetailsdb.getReadableDatabase();
        String query = "insert into user values(?,?,?,?,?)";

        SQLiteStatement statement = userDetailsSqlDB.compileStatement(query);
        statement.bindString(1, FirstName);
        statement.bindString(2, LastName);
        statement.bindString(3, Email);
        statement.bindString(4, MobileNumber);
        if (CountryCode.equals("91"))
            statement.bindString(5, "IN");
        else if (CountryCode.equals("61"))
            statement.bindString(5, "AUS");
        if (CountryCode.equals("64"))
            statement.bindString(5, "NZ");
        long rowId = statement.executeInsert();

        userDetailsSqlDB.close();

    }

    public static boolean islogin() {
        userDetailsSqlDB = userDetailsdb.getReadableDatabase();
        Cursor cursor = userDetailsSqlDB.rawQuery("SELECT * FROM user", null);

        if (cursor != null && cursor.moveToFirst()) {

            cursor.close();
            userDetailsSqlDB.close();
            return true;
        } else {
            Log.e("userId", "noid");

            return false;
        }

    }
    public static void cleardatabase(){
        userDetailsSqlDB = userDetailsdb.getReadableDatabase();
        userDetailsSqlDB.execSQL("delete from user");
        userDetailsSqlDB.close();
    }


}