package com.cabscustomer.extras;

/**
 * Created by Ratnaji on 8/28/2017.
 */

public class DataModel {
    public String name;
    public Integer image;

    // Constructor.
    public DataModel(String name,Integer image) {

        this.name = name;
        this.image=image;
    }
}
