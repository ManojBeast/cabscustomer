package com.cabscustomer.activities;

import android.animation.Animator;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;
import com.cabscustomer.interfaces.SmsListener;

public class ForgotPasswordNew extends AppCompatActivity {

    EditText phoneNumber;
    LinearLayout layout1, layout2, animLay, editMobile, txt_pin_entry_lay, new_password_lay;
    TextView phoneNumberTV;
    Animation alphaAnimation;
    TextView time;
    FrameLayout autoDetect;
    PinEntryEditText txt_pin_entry;
    int[] flagId = {R.drawable.india, R.drawable.australia, R.drawable.new_zealand};
    String[] countryCodes = {"(+91)", "(+61)", "(+64)"};
    String[] country = {"91", "61", "64"};
    String selectedCountry = "91";
    FrameLayout rootLay;

    ImageView flagImage;
    TextView countryCode;
    CountDownTimer countDownTimer;
    String currentOtp = "";
    EditText new_password;
    String phoneNumberString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_new);
        setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        phoneNumberTV = (TextView) findViewById(R.id.phoneNumberTV);
        layout1 = (LinearLayout) findViewById(R.id.layout1);
        layout2 = (LinearLayout) findViewById(R.id.layout2);
        animLay = (LinearLayout) findViewById(R.id.animLay);
        editMobile = (LinearLayout) findViewById(R.id.editMobile);
        txt_pin_entry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);
        txt_pin_entry_lay = (LinearLayout) findViewById(R.id.txt_pin_entry_lay);
        autoDetect = (FrameLayout) findViewById(R.id.autoDetect);
        time = (TextView) findViewById(R.id.time);
        new_password_lay = (LinearLayout) findViewById(R.id.new_password_lay);
        new_password = (EditText) findViewById(R.id.new_password);

        flagImage = (ImageView) findViewById(R.id.flagImage);
        countryCode = (TextView) findViewById(R.id.countryCode);
        rootLay = (FrameLayout) findViewById(R.id.rootLay);


        alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);

        phoneNumber.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        /*countDownTimer = new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                time.setText("DETECTING OTP  \n \n 00:" + millisUntilFinished / 1000);
            }

            public void onFinish() {
               // autoDetect.setVisibility(View.GONE);
                txt_pin_entry_lay.setVisibility(View.VISIBLE);
                txt_pin_entry_lay.startAnimation(alphaAnimation);
                txt_pin_entry.setText("");
            }
        };*/

        txt_pin_entry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (currentOtp.equals(editable.toString())) {
                    AppController.hideKeyboard(ForgotPasswordNew.this);
                    Toast.makeText(ForgotPasswordNew.this, "Entered correct otp", Toast.LENGTH_SHORT).show();
                    txt_pin_entry_lay.setVisibility(View.GONE);
                    new_password_lay.startAnimation(alphaAnimation);
                    new_password_lay.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    public void sendOtp(View view) {
        phoneNumberString = phoneNumber.getText().toString().trim();
        if (phoneNumberString.equals("")) {
            Toast.makeText(this, "Phone Number cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        } else {
            AppController.hideKeyboard(this);
            AppController.showProgres(ForgotPasswordNew.this);
            final String Forgot_Pass = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                    + "&ForMNo=" + phoneNumberString
                    + "&cCode=" + selectedCountry;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Forgot_Pass,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            AppController.stopProgres();
                            Log.i("Response", response);

                            try {
                                if (response.equals("\"1\"")) {
                                    Toast.makeText(ForgotPasswordNew.this, "Mobile Number is Not Registered", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {

                                    currentOtp = response.replace("\"", "");

                                    phoneNumberTV.setText("+" + selectedCountry + " " + phoneNumberString);

                                    Log.i("phoneNumberString", phoneNumberString);

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        int cx = rootLay.getMeasuredWidth() / 2;
                                        int cy = rootLay.getMeasuredHeight() / 2;
                                        float finalRadius = (float) Math.max(rootLay.getWidth(), rootLay.getHeight()) / 2;

                                        Animator anim =
                                                ViewAnimationUtils.createCircularReveal(layout2, cx, cy, 0f, finalRadius);
                                        anim.setInterpolator(new LinearInterpolator());
                                        anim.setDuration(300);

                                        anim.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animator) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animator) {
                                                animLay.setVisibility(View.VISIBLE);
                                                animLay.startAnimation(alphaAnimation);

                                                // countDownTimer.start();

                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animator) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animator) {

                                            }
                                        });
                                        anim.start();
                                    } else {
                                        animLay.setVisibility(View.VISIBLE);
                                        animLay.startAnimation(alphaAnimation);
                                    }
                                    layout2.setVisibility(View.VISIBLE);

                                    layout1.setVisibility(View.GONE);

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppController.stopProgres();
                    Toast.makeText(ForgotPasswordNew.this, "Sending Otp failed", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(stringRequest);
        }


    }

    public void editMobile(View view) {
        // countDownTimer.cancel();
        // autoDetect.setVisibility(View.VISIBLE);
        txt_pin_entry_lay.setVisibility(View.VISIBLE);
        new_password_lay.setVisibility(View.GONE);
        txt_pin_entry.setText("");
        animLay.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            int cx = rootLay.getMeasuredWidth() / 2;
            int cy = rootLay.getMeasuredHeight() / 2;
            float finalRadius = (float) Math.max(rootLay.getWidth(), rootLay.getHeight()) / 2;

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(layout2, cx, cy, finalRadius, 0f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setDuration(300);

            anim.start();
        }
        layout2.setVisibility(View.GONE);
        layout1.setVisibility(View.VISIBLE);

    }

    public void selectCountry(View view) {
        Intent i = new Intent(this, Flags.class);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 1) {
            int position = data.getIntExtra("position", 0);
            flagImage.setImageResource(flagId[position]);
            countryCode.setText(countryCodes[position]);
            selectedCountry = country[position];
            Log.i("data.getIntExtra", "" + data.getIntExtra("position", 0));
        }
    }

    public void changePassword(View view) {
        String newPassword = new_password.getText().toString().trim();
        // Toast.makeText(this, ""+newPassword, Toast.LENGTH_SHORT).show();
        AppController.hideKeyboard(this);
        AppController.showProgres(ForgotPasswordNew.this);
        final String change_pass = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                + "FMNo=" + phoneNumberString
                + "&NewPwd=" + newPassword;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, change_pass,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        AppController.stopProgres();
                        Log.i("Response", response);
                        try {
                            if (response.equals("\"1\"")) {
                                Toast.makeText(ForgotPasswordNew.this, "Your Password is changed successfully", Toast.LENGTH_SHORT).show();
                                finish();
                                return;
                            } else {
                                Toast.makeText(ForgotPasswordNew.this, "Unable to change password", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppController.stopProgres();
                Toast.makeText(ForgotPasswordNew.this, "Unable to change password", Toast.LENGTH_SHORT).show();
                Log.e("Error: ", error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
