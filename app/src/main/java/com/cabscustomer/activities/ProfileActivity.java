package com.cabscustomer.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;

public class ProfileActivity extends AppCompatActivity {
    EditText firstName, lastName, email;
    Button update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.email);
        update = (Button) findViewById(R.id.update);
        final String[] userDetails = AppController.getuserDeatils();
        firstName.setText(userDetails[0]);
        lastName.setText(userDetails[1]);
        email.setText(userDetails[2]);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(update.getText().toString().equals("EDIT")){
                    update.setText("UPDATE");
                    firstName.setEnabled(true);
                    lastName.setEnabled(true);
                    email.setEnabled(true);
                } else if(update.getText().toString().equals("UPDATE")){
                    Toast.makeText(ProfileActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
