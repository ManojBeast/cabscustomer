package com.cabscustomer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;

/**
 * Created by Ratnaji on 8/19/2017.
 */

public class Register extends AppCompatActivity {
    EditText firstName, lastName, emailId, mobileNo, password;
    //Spinner contryselect;
    String firstNameString, lastNameString, emailIdString, mobileNoString, passwordString;
    String responce_otp;

    int[] flagId = {R.drawable.india, R.drawable.australia, R.drawable.new_zealand};
    String[] countryCodes = {"(+91)", "(+61)", "(+64)"};
    String[] country = {"91", "61", "64"};
    String selectedCountry = "91";

    ImageView flagImage;
    TextView countryCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registerpage);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        emailId = (EditText) findViewById(R.id.emailid);
        mobileNo = (EditText) findViewById(R.id.mobNumber);
        password = (EditText) findViewById(R.id.password);

        flagImage = (ImageView) findViewById(R.id.flagImage);
        countryCode = (TextView) findViewById(R.id.countryCode);

        //firstName.setError("This field can not be blank");
    }

    public void back(View view) {
        finish();
    }

    public void register(View view) {
        if (true) {

            firstNameString = firstName.getText().toString().trim();
            lastNameString = lastName.getText().toString().trim();
            mobileNoString = mobileNo.getText().toString().trim();
            emailIdString = emailId.getText().toString().trim();
            passwordString = password.getText().toString().trim();

            AppController.showProgres(Register.this);
            final String Mob_verify = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                    + "rmNum=" + mobileNoString
                    + "&cCode=" + selectedCountry;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Mob_verify,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            AppController.stopProgres();
                            Log.i("Response", response);
                            try {
                                if (response.equals("\"1\"")) {
                                    Toast.makeText(Register.this, "Mobile Number Already exist", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    responce_otp = response.replace("\"", "");
                                    View v = View.inflate(Register.this, R.layout.register_otp, null);
                                    final AlertDialog dialog = new AlertDialog.Builder(Register.this).setView(v, 0, 0, 0, 0).create();
                                    Button fab = (Button) v.findViewById(R.id.done);
                                    final EditText reg_otp = (EditText) v.findViewById(R.id.reg_otp);
                                    fab.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String reg_otp1 = reg_otp.getText().toString();
                                            if (reg_otp1.equals(responce_otp)) {
                                                AppController.showProgres(Register.this);
                                                final String REGISTER_URL = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                                                        + "fN=" + Uri.encode(firstNameString)
                                                        + "&LastName=" + Uri.encode(lastNameString)
                                                        + "&eml=" + emailIdString
                                                        + "&rMno=" + mobileNoString
                                                        + "&Password=" + passwordString
                                                        + "&countrycode=" + selectedCountry;

                                                Log.i("REGISTER_URL", REGISTER_URL);
                                                StringRequest req = new StringRequest(Request.Method.GET, REGISTER_URL,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                AppController.stopProgres();
                                                                Log.e("Response:", response);
                                                                try {
//                                                                    Long responseDouble = Long.parseLong(response.getJSONObject(0).getString("Result"));
                                                                    if (response.equals("\"-1\"")) {
                                                                        Toast.makeText(Register.this, "Mobile Number already Registered", Toast.LENGTH_SHORT).show();
                                                                        return;
                                                                    } else if (response.equals("\"-2\"")) {
                                                                        Toast.makeText(Register.this, "Email is already Registered", Toast.LENGTH_SHORT).show();
                                                                    } else {
                                                                        //Intent i = new Intent(Register.this, Login.class);
                                                                        //startActivity(i);
                                                                        Register.this.finish();
                                                                        Toast.makeText(Register.this, "Registration successful, Login to continue", Toast.LENGTH_LONG).show();
                                                                    }
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        AppController.stopProgres();
                                                        Toast.makeText(Register.this, "Registration failed", Toast.LENGTH_SHORT).show();
                                                        Log.e("Error: ", error.getMessage());
                                                    }
                                                });

                                                AppController.getInstance().addToRequestQueue(req);
                                            }
                                        }
                                    });
                                    dialog.show();

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppController.stopProgres();
                    Toast.makeText(Register.this, "Sending Otp failed", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(stringRequest);


        }
    }

    /*private void resetFields() {
        firstName.setText("");
        lastName.setText("");
        mobileNo.setText("");
        emailId.setText("");
        password.setText("");
    }*/

    private boolean isValid() {

        firstNameString = firstName.getText().toString().trim();
        lastNameString = lastName.getText().toString().trim();
        mobileNoString = mobileNo.getText().toString().trim();
        emailIdString = emailId.getText().toString().trim();
        passwordString = password.getText().toString().trim();

        //return true;

        if (firstNameString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field can not be blank", Toast.LENGTH_LONG).show();
//            firstName.setError("This field can not be blank");
            return false;
        } else if (!(firstNameString.matches(".*[a-zA-Z]+.*"))) {
            Toast.makeText(getApplicationContext(), "First name must contain atleast an alphabet", Toast.LENGTH_LONG).show();
//            firstName.setError("First name must contain atleast an alphabet");
            return false;
        } else if (!(firstNameString.matches("^[A-Z a-z 0-9 .]*$"))) {
            Toast.makeText(getApplicationContext(), "First name should not contain special characters", Toast.LENGTH_LONG).show();
//            firstName.setError("First name should not contain special characters");
            return false;
        } else if (Character.isDigit(firstNameString.charAt(0))) {
            Toast.makeText(getApplicationContext(), "First name cannot start with number", Toast.LENGTH_LONG).show();
//            firstName.setError("First name cannot start with number");
            return false;
        } else if (lastNameString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field cannot be blank", Toast.LENGTH_LONG).show();
//            lastName.setError("This field can not be blank");
            return false;
        } else if (!(lastNameString.matches(".*[a-zA-Z]+.*"))) {
            Toast.makeText(getApplicationContext(), "Last name must contain atleast an alphabet", Toast.LENGTH_LONG).show();
//            lastName.setError("Last name must contain atleast an alphabet");
            return false;
        } else if (!(lastNameString.matches("^[A-Z a-z 0-9 .]*$"))) {
            Toast.makeText(getApplicationContext(), "Last name should not contain special characters", Toast.LENGTH_LONG).show();
//            lastName.setError("Last name should not contain special characters");
            return false;
        } else if (Character.isDigit(lastNameString.charAt(0))) {
            Toast.makeText(getApplicationContext(), "Last name cannot start with number", Toast.LENGTH_LONG).show();
//            lastName.setError("Last name cannot start with number");
            return false;
        } else if (mobileNoString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field cannot be blank", Toast.LENGTH_LONG).show();
//            mobileNo.setError("This field can not be blank");
            return false;
        } else if (!(mobileNoString.length() == 10)) {
            Toast.makeText(getApplicationContext(), "This field can not be blank", Toast.LENGTH_LONG).show();
            mobileNo.setError("Mobile number entered is not valid");
            return false;
        } else if ((Integer.parseInt("" + mobileNoString.charAt(0)) < 7)) {
            Toast.makeText(getApplicationContext(), "Mobile number entered is not valid", Toast.LENGTH_LONG).show();
//            mobileNo.setError("Mobile number entered is not valid");
            return false;
        } else if (emailIdString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field cannot be blank", Toast.LENGTH_LONG).show();
//            emailId.setError("This field can not be blank");
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailIdString).matches()) {
            Toast.makeText(getApplicationContext(), "Email id entered is not valid", Toast.LENGTH_LONG).show();
//            emailId.setError("Email id entered is not valid");
            return false;
        } else if (passwordString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field cannot be blank", Toast.LENGTH_LONG).show();
//            password.setError("This field can not be blank");
            return false;
        } else if ((passwordString.matches(".*[*$]+.*"))) {
            Toast.makeText(getApplicationContext(), "Password must not contain * and $", Toast.LENGTH_LONG).show();
//            password.setError("Password must not contain * and $");
            return false;
        } else if ((passwordString.length()) < 5) {
            Toast.makeText(getApplicationContext(), "Password minimum length must be 5 characters", Toast.LENGTH_LONG).show();
//            password.setError("Password minimum length must be 5 characters");
            return false;
        } else {
//            firstName.setError(null);
//            mobileNo.setError(null);
//            lastName.setError(null);
//            password.setError(null);
//            emailId.setError(null);
            return true;
        }
    }

    public void selectCountry(View view) {
        Intent i = new Intent(this, Flags.class);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 1) {
            int position = data.getIntExtra("position", 0);
            flagImage.setImageResource(flagId[position]);
            countryCode.setText(countryCodes[position]);
            selectedCountry = country[position];
            Log.i("data.getIntExtra", "" + data.getIntExtra("position", 0));
        }
    }
}
