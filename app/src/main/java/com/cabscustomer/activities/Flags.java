package com.cabscustomer.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cabscustomer.R;
import com.cabscustomer.adaptors.FlagsAdaptor;

public class Flags extends AppCompatActivity {
    RecyclerView flagaRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flags);
        setTitle("Select Country");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        flagaRv = (RecyclerView) findViewById(R.id.flagaRv);
        flagaRv.setLayoutManager(new LinearLayoutManager(this));
        flagaRv.setAdapter(new FlagsAdaptor(this));
    }
}
