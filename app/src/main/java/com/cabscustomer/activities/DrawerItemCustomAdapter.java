package com.cabscustomer.activities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cabscustomer.R;
import com.cabscustomer.extras.DataModel;

/**
 * Created by Ratnaji on 8/28/2017.
 */

public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel> {

    Context mContext;
    int layoutResourceId;
    DataModel data[] = null;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DataModel[] data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);
        ImageView imageView=(ImageView) listItem.findViewById(R.id.imageViewIcon);

        DataModel folder = data[position];

        textViewName.setText(folder.name);
//        textViewName.setCompoundDrawables(mContext.getResources().getDrawable(folder.image,null),null,null,null);
        imageView.setImageResource(folder.image);
        return listItem;
    }
}

