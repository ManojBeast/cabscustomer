package com.cabscustomer.activities;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import com.cabscustomer.transformers.CustomTransformer;
import com.cabscustomer.R;

public class Intro extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    FrameLayout rootLay;
    int[] colors = new int[]{Color.parseColor("#ff0054a5"), Color.parseColor("#fff55152"), Color.parseColor("#388e3c"), Color.parseColor("#FF7043")};
    ArgbEvaluator argbEvaluator;
    TabLayout tabDots;
    ImageView next;
    static Intro activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        activity = this;

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        rootLay = (FrameLayout) findViewById(R.id.rootLay);
        tabDots = (TabLayout) findViewById(R.id.tabDots);
        next = (ImageView) findViewById(R.id.next);
        argbEvaluator = new ArgbEvaluator();
        final HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.scroll_view);

        mViewPager.setPageTransformer(true, new CustomTransformer());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int x = (int) ((mViewPager.getWidth() * position + positionOffsetPixels) * 0.35f);
                scrollView.scrollTo(x, 0);
                if (position >= Intro.this.mSectionsPagerAdapter.getCount() - 1 || position >= Intro.this.colors.length - 1) {
                    Intro.this.rootLay.setBackgroundColor(Intro.this.colors[Intro.this.colors.length - 1]);
                    return;
                }
                Integer colorValue = ((Integer) Intro.this.argbEvaluator.evaluate(positionOffset, Intro.this.colors[position], Intro.this.colors[position + 1]));
                Intro.this.rootLay.setBackgroundColor(colorValue);
                if (Build.VERSION.SDK_INT >= 21) {
                    getWindow().setStatusBarColor(colorValue);
                }

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3)
                    next.setImageResource(R.drawable.tick);
                else
                    next.setImageResource(R.drawable.next_arrow);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        mViewPager.setAdapter(mSectionsPagerAdapter);
        //mViewPager.setOffscreenPageLimit(4);
        tabDots.setupWithViewPager(mViewPager, false);

    }

    public void skip(View view) {
        Intent login_intent=new Intent(this,Login.class);
        startActivity(login_intent);

//        Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
    }

    public void next(View view) {
        if (mViewPager.getCurrentItem() == 3) {
            Intent login_intent=new Intent(this,Login.class);
            startActivity(login_intent);
//            Toast.makeText(this, "under development", Toast.LENGTH_SHORT).show();
        } else {
            mViewPager.beginFakeDrag();
            //mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
            //mViewPager.endFakeDrag();

            Handler handler = new Handler();
            handler.post(new PageTurner(handler, mViewPager));
        }
    }

    private static class PageTurner implements Runnable {
        private final Handler handler;
        private final ViewPager pager;
        private int count = 0;

        private PageTurner(Handler handler, ViewPager pager) {
            this.handler = handler;
            this.pager = pager;
        }

        @Override
        public void run() {
            if (pager.isFakeDragging()) {
                if (count < 200) {
                    count++;
                    pager.fakeDragBy(-count * count);
                    handler.postDelayed(this, 1);
                } else {
                    pager.endFakeDrag();
                }
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";
        int[] phoneImages = {R.drawable.intro_one, R.drawable.intro_two, R.drawable.intro_three, R.drawable.intro_four};

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_intro, container, false);
            ImageView phoneImg = (ImageView) rootView.findViewById(R.id.phoneImg);
            Log.i("Integer.parseInt(getSt", "" + getArguments().getInt(ARG_SECTION_NUMBER));
            phoneImg.setImageResource(phoneImages[getArguments().getInt(ARG_SECTION_NUMBER)]);
            return rootView;
        }

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {

            super.setUserVisibleHint(isVisibleToUser);
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
