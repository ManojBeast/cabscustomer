package com.cabscustomer.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by Ratnaji on 8/8/2017.
 */

public class Login extends AppCompatActivity {
    private EditText mobileNo, password;
    String mobileNoString, passwordString;
    GoogleApiClient googleApiClient;
    String familyName, name, email;
    private static final int GOOGLE_REQUEST_CODE = 1, FACEBOOK_REQUEST_CODE = 2;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    String mobileNumber;
    EditText mobile;
    ImageButton fbLogin;
    LoginButton loginButton;
    String responce_otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext(),FACEBOOK_REQUEST_CODE);
        setContentView(R.layout.loginpage);
        mobileNo = (EditText) findViewById(R.id.input_mobileNumber);
        password = (EditText) findViewById(R.id.input_password);

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, null).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();

//        fbLogin= (ImageButton) findViewById(R.id.fbLogin);
        loginButton= (LoginButton) findViewById(R.id.login_button);

        callbackManager=CallbackManager.Factory.create();
        accessTokenTracker=new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                AccessToken.getCurrentAccessToken().getPermissions();
            }
        };
        profileTracker =new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                displayMessage(currentProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        loginButton.setReadPermissions(Arrays.asList("email"));
        loginButton.setReadPermissions(Arrays.asList("user_status"));
        loginButton.registerCallback(callbackManager,callback);

          }

    private FacebookCallback<LoginResult> callback=new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            Log.e("Login success \n",loginResult.getAccessToken().getUserId()+"\n"+loginResult.getAccessToken().getToken());
//            AccessToken accessToken=loginResult.getAccessToken();
            Profile profile=Profile.getCurrentProfile();
            displayMessage(profile);

        }

        @Override
        public void onCancel() {
            Log.e("Result:","FB login cancelled");

        }

        @Override
        public void onError(FacebookException error) {
            Log.e("Result:","FB login Error");
            error.printStackTrace();
        }
    };

    private void displayMessage(Profile profile){
        if(profile != null){
            String profileName=profile.getName();
            Log.e("fb Profilename",profileName);
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayMessage(profile);
    }

    public void register(View view) {
        Intent i = new Intent(Login.this, Register.class);
        startActivity(i);
    }

    public void login(View view) {
        if (true) {
            AppController.showProgres(this);
            mobileNoString = mobileNo.getText().toString().trim();
            passwordString = password.getText().toString().trim();
            final String REGISTER_URL = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                    + "&MN=" + mobileNoString
                    + "&PWD=" + passwordString
                    + "&gm=";

            Log.i("REGISTER_URL", REGISTER_URL);
            JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, REGISTER_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            AppController.stopProgres();
                            Log.e("Response:", response.toString());
                            try {
                                if (response.getJSONObject(0).has("Output") && response.getJSONObject(0).getString("Output").equals("0")) {
                                    Toast.makeText(Login.this, "Login failed", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Toast.makeText(Login.this, "Login successful", Toast.LENGTH_SHORT).show();
                                JSONObject userDetailsJson = response.getJSONObject(0);
                                AppController.insertUserDetails(userDetailsJson);
                                Intent i = new Intent(Login.this, MapsActivity.class);
                                startActivity(i);
                                Intro.activity.finish();
                                Login.this.finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppController.stopProgres();
                    Toast.makeText(Login.this, "Login failed", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", error.getMessage());
                }
            });

            AppController.getInstance().addToRequestQueue(req);
        } else {
            Toast.makeText(this, "Please enter valid details", Toast.LENGTH_SHORT).show();
        }

    }

    public void forgotPassword(View view) {
        Intent i = new Intent(this, ForgotPasswordNew.class);
        startActivity(i);
    }


    private void resetFields() {
        mobileNo.setText("");
        password.setText("");
    }


    private boolean isValid() {
        mobileNoString = mobileNo.getText().toString().trim();
        passwordString = password.getText().toString().trim();

//        return true;
        if (mobileNoString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field can not be blank", Toast.LENGTH_LONG).show();
//            mobileNo.setError("This field can not be blank");
            return false;
        } else if (!(mobileNoString.length() == 10)) {
            Toast.makeText(getApplicationContext(), "Mobile number entered is not valid", Toast.LENGTH_LONG).show();
//            mobileNo.setError("Mobile number entered is not valid");
            return false;
        } else if ((Integer.parseInt("" + mobileNoString.charAt(0)) < 7)) {
            Toast.makeText(getApplicationContext(), "Mobile number entered is not valid", Toast.LENGTH_LONG).show();
//            mobileNo.setError("Mobile number entered is not valid");
            return false;
        } else if (passwordString.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "This field can not be blank", Toast.LENGTH_LONG).show();
//            password.setError("This field can not be blank");
            return false;
        } else if ((passwordString.matches(".*[*$]+.*"))) {
            Toast.makeText(getApplicationContext(), "Password must not contain * and $", Toast.LENGTH_LONG).show();
//            password.setError("Password must not contain * and $");
            return false;
        } else if ((passwordString.length()) < 5) {
            Toast.makeText(getApplicationContext(), "Password minimum length must be 5 characters", Toast.LENGTH_LONG).show();
//            password.setError("Password minimum length must be 5 characters");
            return false;
        } else {
//            mobileNo.setError(null);
//            password.setError(null);
            return true;
        }
    }


    public void back(View view) {
        finish();
    }

    public void GoogleLogin(View view) {
        Intent googleLogin = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(googleLogin, GOOGLE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_REQUEST_CODE && resultCode == RESULT_OK) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.i("Result", result.toString());
            handleResult(result);
        }else if(requestCode == FACEBOOK_REQUEST_CODE && resultCode == RESULT_OK){
            callbackManager.onActivityResult(requestCode,resultCode,data);
        }
    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            name = account.getDisplayName();
            email = account.getEmail();
            familyName = account.getFamilyName();
            Log.i("name1", name);
            Log.i("familyName1", familyName);
            Log.i("email1", email);
            AppController.showProgres(this);
            final String GMAIL_URL = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?gm=" + email + "&ltId=1";
            Log.i("GMAIL_URL", GMAIL_URL);
            JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, GMAIL_URL, null,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            AppController.stopProgres();
                            Log.e("Response:", response.toString());
                            try {
                                if (response.getJSONObject(0).has("Output") && response.getJSONObject(0).getString("Output").equals("-2")) {
//                                    Intent intent = new Intent(Login.this, MobileVerification.class);
//                                    intent.putExtra("familName",familyName);
//                                    intent.putExtra("name",name);
//                                    intent.putExtra("email",email);
//                                    startActivity(intent);
                                    View v = View.inflate(Login.this, R.layout.forgot_password, null);
                                    final AlertDialog dialog = new AlertDialog.Builder(Login.this).setView(v, 0, 0, 0, 0).create();
                                    Button fab = (Button) v.findViewById(R.id.done);
                                    mobile = (EditText) v.findViewById(R.id.mobile);

                                    fab.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mobileNumber = mobile.getText().toString();
                                            if (mobileNumber.equals("")) {
                                                Snackbar.make(v, "Mobile number cannot be empty", Snackbar.LENGTH_LONG).show();
                                            } else {
                                                AppController.showProgres(Login.this);
                                                final String Forgot_Pass = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                                                        + "rmNum=" + mobileNumber;
                                                StringRequest stringRequest = new StringRequest(Request.Method.GET, Forgot_Pass,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response) {
                                                                AppController.stopProgres();
                                                                Log.i("Response", response);
                                                                try {
                                                                    if (response.equals("\"1\"")) {
                                                                        Toast.makeText(Login.this, "Mobile Number Already exist", Toast.LENGTH_SHORT).show();
                                                                        return;
                                                                    } else {
                                                                        responce_otp = response.replace("\"", "");
                                                                        View v = View.inflate(Login.this, R.layout.register_otp, null);
                                                                        final AlertDialog dialog = new AlertDialog.Builder(Login.this).setView(v, 0, 0, 0, 0).create();
                                                                        Button fab = (Button) v.findViewById(R.id.done);
                                                                        final EditText reg_otp = (EditText) v.findViewById(R.id.reg_otp);
                                                                        fab.setOnClickListener(new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View v) {
                                                                                String reg_otp1 = reg_otp.getText().toString();
                                                                                if (reg_otp1.equals(responce_otp)) {
                                                                                    AppController.showProgres(Login.this);
                                                                                    //http://hotfindzdev.com/ryder/RyderServices/CustomerDetails.aspx?fN=akhil&LastName=reddy&eml=akvi@gmail.com&rMno=1904050449&ltId=1&countrycode=+91
                                                                                    final String REGISTER_URL = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"
                                                                                            + "fN=" + Uri.encode(name)
                                                                                            + "&LastName=" + Uri.encode(familyName)
                                                                                            + "&eml=" + email
                                                                                            + "&rMno=" + mobileNoString
                                                                                            //+ "&Password=" + passwordString
                                                                                            + "&ltId=1"
                                                                                            + "&countrycode=+91";

                                                                                    Log.i("REGISTER_URL", REGISTER_URL);
                                                                                    StringRequest req = new StringRequest(Request.Method.GET, REGISTER_URL,
                                                                                            new Response.Listener<String>() {
                                                                                                @Override
                                                                                                public void onResponse(String response) {
                                                                                                    AppController.stopProgres();
                                                                                                    Log.e("Response:", response);
                                                                                                    try {
//                                                                    Long responseDouble = Long.parseLong(response.getJSONObject(0).getString("Result"));
                                                                                                        if (response.equals("\"-1\"")) {
                                                                                                            Toast.makeText(Login.this, "Mobile Number already Registered", Toast.LENGTH_SHORT).show();
                                                                                                            return;
                                                                                                        } else if (response.equals("\"-2\"")) {
                                                                                                            Toast.makeText(Login.this, "Email is already Registered", Toast.LENGTH_SHORT).show();
                                                                                                        } else {
                                                                                                            Intent i = new Intent(Login.this, MapsActivity.class);
                                                                                                            startActivity(i);
                                                                                                            Login.this.finish();
//                                                                                                            Toast.makeText(Login.this, "Registration successful, Login to continue", Toast.LENGTH_LONG).show();
                                                                                                        }
                                                                                                    } catch (Exception e) {
                                                                                                        e.printStackTrace();
                                                                                                    }
                                                                                                }
                                                                                            }, new Response.ErrorListener() {
                                                                                        @Override
                                                                                        public void onErrorResponse(VolleyError error) {
                                                                                            AppController.stopProgres();
                                                                                            Toast.makeText(Login.this, "Registration failed", Toast.LENGTH_SHORT).show();
                                                                                            Log.e("Error: ", error.getMessage());
                                                                                        }
                                                                                    });

                                                                                    AppController.getInstance().addToRequestQueue(req);
                                                                                }
                                                                            }
                                                                        });
                                                                        dialog.show();
//                                                                        dialog.cancel();
//                                                                        Intent dialogIntent=new Intent(Login.this,ForgotPasswordOtp.class);
//                                                                        dialogIntent.putExtra("MobileNumber",mobileNumber);
//                                                                        dialogIntent.putExtra("Otp",response.replace("\"",""));
//                                                                        startActivity(dialogIntent);
                                                                    }
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                            }
                                                        }, new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        AppController.stopProgres();
                                                        Toast.makeText(Login.this, "Sending Otp failed", Toast.LENGTH_SHORT).show();
                                                        Log.e("Error: ", error.getMessage());
                                                    }
                                                });
                                                AppController.getInstance().addToRequestQueue(stringRequest);
                                            }
                                        }
                                    });
                                    dialog.show();

                                }
//                                Toast.makeText(Login.this, "Login successful", Toast.LENGTH_SHORT).show();
                                else {
                                    JSONObject userDetailsJson = response.getJSONObject(0);
                                    AppController.insertUserDetails(userDetailsJson);

                                    Intent i = new Intent(Login.this, MapsActivity.class);
                                    startActivity(i);
                                    Intro.activity.finish();
                                    Login.this.finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AppController.stopProgres();
                    Toast.makeText(Login.this, "Login failed", Toast.LENGTH_SHORT).show();
                    Log.e("Error: ", error.getMessage());
                }
            });
            AppController.getInstance().addToRequestQueue(req);


        }
    }

}


