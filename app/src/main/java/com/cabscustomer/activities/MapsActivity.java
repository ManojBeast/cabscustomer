package com.cabscustomer.activities;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;
import com.cabscustomer.extras.DataModel;
import com.cabscustomer.mapModules.DirectionFinder;
import com.cabscustomer.mapModules.DirectionFinderListener;
import com.cabscustomer.mapModules.Route;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, DirectionFinderListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "gps";
    private GoogleMap mMap;
    private ArrayList<LatLng> wayPoints = new ArrayList();
    TextView starting, destination;
    protected GoogleApiClient mGoogleApiClient;
    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
            new LatLng(18.214761, 67.832765),
            new LatLng(32.060561, 88.365206));
    //PlaceAutoCompleteAdapter mAdapter;
    LatLng from = null, to = null;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    Marker marker = null, markerStart = null, markerEnd = null;
    Location currentLocation;
    double latitude, longitude;
    LatLng latLngStart, latLngEnd, myLocation;

    ImageView back;
    CardView searchLay;
    FloatingActionButton myLocationFab;

    Animation searchInAnim, searchoutAnim, fabInAnim, fabOutAnim, backInAnim, backOutAnim, bottomInAnim, bottomOutAnim, carPick;
    View tripOptions;
    TextView distance, time;
    FrameLayout mapFrame;
    TextView infoCab;
    ImageView cabImagePrev = null;
    private boolean isMarkerRotating = false;
    String startAddress, endAdress;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        starting = (TextView) findViewById(R.id.start);
        destination = (TextView) findViewById(R.id.destination);


        back = (ImageView) findViewById(R.id.back);
        searchLay = (CardView) findViewById(R.id.searchLay);
        myLocationFab = (FloatingActionButton) findViewById(R.id.myLocationFab);
        tripOptions = findViewById(R.id.tripOptions);
        distance = (TextView) findViewById(R.id.distance);
        time = (TextView) findViewById(R.id.time);
        mapFrame = (FrameLayout) findViewById(R.id.mapFrame);
        infoCab = (TextView) findViewById(R.id.infoCab);

        searchInAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        searchoutAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_bottom);
        fabInAnim = AnimationUtils.loadAnimation(this, R.anim.fab_in);
        fabOutAnim = AnimationUtils.loadAnimation(this, R.anim.fab_out);
        backInAnim = AnimationUtils.loadAnimation(this, R.anim.back_in);
        backOutAnim = AnimationUtils.loadAnimation(this, R.anim.back_out);
        bottomInAnim = AnimationUtils.loadAnimation(this, R.anim.in_bottom);
        bottomOutAnim = AnimationUtils.loadAnimation(this, R.anim.out_bottom);
        carPick = AnimationUtils.loadAnimation(this, R.anim.car_pick);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        DataModel[] drawerItem = new DataModel[8];

        drawerItem[0] = new DataModel("PROFILE", R.drawable.profile_icon);
        drawerItem[1] = new DataModel("PAYMENT", R.drawable.payment_icon);
        drawerItem[2] = new DataModel("PROMOTIONS", R.drawable.promotion_icon);
        drawerItem[3] = new DataModel("SHARE", R.drawable.share_icon);
        drawerItem[4] = new DataModel("SUPPORT", R.drawable.support_icon);
        drawerItem[5] = new DataModel("ABOUT", R.drawable.about_icon);
        drawerItem[6] = new DataModel("BOOKING HISTORY", R.drawable.booking_icon);
        drawerItem[7] = new DataModel("LOGOUT", R.drawable.logout_icon);

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectItem(i);
            }
        });
//        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerLayout.setDrawerListener(mDrawerToggle);

        findViewById(R.id.taxi).performClick();

        mapFragment.getMapAsync(this);

        starting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity(1);
            }
        });
        destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openAutocompleteActivity(2);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cabImagePrev = null;
                mapFrame.animate().setInterpolator(new LinearInterpolator()).setDuration(1500);

                tripOptions.setVisibility(View.GONE);
                tripOptions.startAnimation(bottomOutAnim);

                back.setVisibility(View.GONE);
                back.startAnimation(backOutAnim);
                searchLay.setVisibility(View.VISIBLE);
                searchLay.startAnimation(searchInAnim);
                myLocationFab.setVisibility(View.VISIBLE);
                myLocationFab.startAnimation(fabInAnim);
                destination.setText("Choose a destination...");
                markerEnd = null;
                mMap.clear();

                showMe();

            }
        });


    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        if (back.getVisibility() == View.VISIBLE)
            back.performClick();
        else
            super.onBackPressed();
    }

    private void signin() {

    }


    public void DrawerIcon(View view) {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }

    private void openAutocompleteActivity(int REQUEST_CODE) {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS)
                    .setCountry(AppController.getuserDeatils()[4])
                    .build();

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    /*.setBoundsBias(new LatLngBounds(
                            new LatLng(18.214761, 67.832765),
                            new LatLng(32.060561, 88.365206)))*/
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e("sdsds", message);
            //Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Check that the result was from the autocomplete widget.
        if (requestCode == 1 || requestCode == 2) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);
                String[] latLang = String.valueOf(place.getLatLng()).split(",");
                latitude = Double.parseDouble(latLang[0].replace("lat/lng: (", ""));
                longitude = Double.parseDouble(latLang[1].replace(")", ""));
                String fullName = (String) place.getAddress();
                Log.i("sdsdssd", "Place Selected: " + fullName);
                if (requestCode == 1) {
                    starting.setText(fullName);
                    latLngStart = new LatLng(latitude, longitude);
                    if (markerStart != null)
                        markerStart.remove();
                    markerStart = mMap.addMarker(new MarkerOptions().position(latLngStart));
                    markerStart.setAnchor(0.2f, 1f);
                    markerStart.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.pickup_location, fullName.split(",")[0])));
                    startAddress = fullName.split(",")[0];
                    if (markerStart != null && markerEnd != null) {
                        try {
                            new DirectionFinder(this, latLngStart, latLngEnd).execute();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngStart));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
                    }

                }
                if (requestCode == 2) {
                    destination.setText(fullName);
                    latLngEnd = new LatLng(latitude, longitude);
                    if (markerEnd != null)
                        markerEnd.remove();
                    markerEnd = mMap.addMarker(new MarkerOptions().position(latLngEnd));
                    markerEnd.setAnchor(0.2f, 1f);
                    markerEnd.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.drop_location, fullName.split(",")[0])));
                    endAdress = fullName.split(",")[0];
                    if (markerStart != null && markerEnd != null) {
                        try {
                            new DirectionFinder(this, latLngStart, latLngEnd).execute();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLngEnd));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
                    }

                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("sdsdsdd", "Error: Status = " + status.toString());

            } else if (resultCode == RESULT_CANCELED) {

            }
        } else if (requestCode == 3) {
            Log.e("my location", "success");
            showMe();
            showMe();
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    private void sendRequest() {
        String origin = "Hyderabad";
        String destination = "Vijayawada";
        try {
            //new DirectionFinder(this, origin, destination).execute();
            from = new LatLng(17.4354941, 78.4131063);
            to = new LatLng(17.4175501, 78.4462803);

            wayPoints.add(new LatLng(17.4126274, 78.2679587));
            wayPoints.add(new LatLng(17.3864341, 78.4385143));
            wayPoints.add(new LatLng(17.3856051, 78.4333643));

            new DirectionFinder(this, from, to).execute();//.setWayPoints(wayPoints).execute();//use either string or LatLang

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromVectorDrawable(Context context, int drawableId, String title) {
        LinearLayout tv = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_dialog, null, false);
        tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        tv.layout(0, 0, tv.getMeasuredWidth(), tv.getMeasuredHeight());
        ImageView markerIcon = (ImageView) tv.findViewById(R.id.markerIcon);
        TextView titleTv = (TextView) tv.findViewById(R.id.title);
        titleTv.setText(title);
        markerIcon.setImageResource(drawableId);
        tv.setDrawingCacheEnabled(true);
        tv.buildDrawingCache();
        Bitmap bm = tv.getDrawingCache();
        return bm;
    }

    public Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }


    public void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;
            float deltaRotation = Math.abs(toRotation - startRotation) % 360;
            final float rotation = (deltaRotation > 180 ? 360 - deltaRotation : deltaRotation) * ((toRotation - startRotation >= 0 && toRotation - startRotation <= 180)
                    || (toRotation - startRotation <= -180 && toRotation - startRotation >= -360) ? 1 : -1);

            final LinearInterpolator interpolator = new LinearInterpolator();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);
                    marker.setRotation((startRotation + t * rotation) % 360);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }

    }

    public void animateMarker(final List<LatLng> polyLineList, final Marker cabMarker) {
        final Handler handler = new Handler();
        final int[] index = {-1};
        final int[] next = {1};
        final LatLng[] startPosition = new LatLng[1];
        final LatLng[] endPosition = new LatLng[1];
        final float[] v = new float[1];
        final Double[] lng = new Double[1];
        final Double[] lat = new Double[1];


        final Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (index[0] < polyLineList.size() - 1) {
                    index[0]++;
                    next[0] = index[0] + 1;
                }
                if (index[0] < polyLineList.size() - 1) {
                    startPosition[0] = polyLineList.get(index[0]);
                    endPosition[0] = polyLineList.get(next[0]);
                }

                if(index[0] == polyLineList.size() - 1)
                    return;

                Log.i("animateIndex",""+index[0]+"   polySize"+polyLineList.size());

                TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
                    @Override
                    public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                        rotateMarker(cabMarker, (float) bearingBetweenLocations(startValue, endValue));
                        double lat = (endValue.latitude - startValue.latitude) * fraction + startValue.latitude;
                        double lngDelta = endValue.longitude - startValue.longitude;

                        // Take the shortest path across the 180th meridian.
                        if (Math.abs(lngDelta) > 180) {
                            lngDelta -= Math.signum(lngDelta) * 360;
                        }
                        double lng = lngDelta * fraction + startValue.longitude;
                        //cabMarker.setRotation(getBearing(startValue, endValue));
                        return new LatLng(lat, lng);
                    }
                };
                ObjectAnimator animator = ObjectAnimator.ofObject(cabMarker, property, typeEvaluator, endPosition[0]);
                animator.setDuration(3000);
                animator.start();
                //animator.cancel();
                handler.postDelayed(this, 3000);
            }
        }, 300);
    }
//    public void animateMarker(final List<LatLng> polyLineList, final Marker cabMarker) {
//        final Handler handler = new Handler();
//        final int[] index = {-1};
//        final int[] next = {1};
//        final LatLng[] startPosition = new LatLng[1];
//        final LatLng[] endPosition = new LatLng[1];
//        final float[] v = new float[1];
//        final Double[] lng = new Double[1];
//        final Double[] lat = new Double[1];
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (index[0] < polyLineList.size() - 1) {
//                    index[0]++;
//                    next[0] = index[0] + 1;
//                }
//                if (index[0] < polyLineList.size() - 1) {
//                    startPosition[0] = polyLineList.get(index[0]);
//                    endPosition[0] = polyLineList.get(next[0]);
//                }
//
//                Log.i("animateIndex",""+index[0]+"   polySize"+polyLineList.size());
//
//                TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
//                    @Override
//                    public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
//                        rotateMarker(cabMarker, (float) bearingBetweenLocations(startValue, endValue));
//                        double lat = (endValue.latitude - startValue.latitude) * fraction + startValue.latitude;
//                        double lngDelta = endValue.longitude - startValue.longitude;
//
//                        // Take the shortest path across the 180th meridian.
//                        if (Math.abs(lngDelta) > 180) {
//                            lngDelta -= Math.signum(lngDelta) * 360;
//                        }
//                        double lng = lngDelta * fraction + startValue.longitude;
//                        //cabMarker.setRotation(getBearing(startValue, endValue));
//                        return new LatLng(lat, lng);
//                    }
//                };
//                Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");
//                ObjectAnimator animator = ObjectAnimator.ofObject(cabMarker, property, typeEvaluator, endPosition[0]);
//                animator.setDuration(100);
//                animator.start();
//                //animator.cancel();
//                handler.postDelayed(this, 100);
//            }
//        }, 300);
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(false);
        showMe();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (marker.equals(markerStart)) {
                    starting.performClick();
                } else if (marker.equals(markerEnd)) {
                    destination.performClick();
                }

                return true;
            }
        });

    }


    @Override
    public void onDirectionFinderStart() {

    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes, String data) {
        if (routes.isEmpty()) {
            Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
            return;
        }

        mMap.clear();

        Route route = routes.get(0);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(route.startLocation);
        builder.include(route.endLocation);
        markerStart = mMap.addMarker(new MarkerOptions().position(latLngStart));
        markerStart.setAnchor(0.2f, 1f);
        markerStart.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.pickup_location, starting.getText().toString().split(",")[0])));
        startAddress = starting.getText().toString().split(",")[0];

        markerEnd = mMap.addMarker(new MarkerOptions().position(latLngEnd));
        markerEnd.setAnchor(0.2f, 1f);
        markerEnd.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.drop_location, destination.getText().toString().split(",")[0])));
        endAdress = destination.getText().toString().split(",")[0];

        //mapFrame.animate().setInterpolator(new LinearInterpolator()).setDuration(1500);


        tripOptions.setVisibility(View.VISIBLE);
        tripOptions.startAnimation(bottomInAnim);

        back.setVisibility(View.VISIBLE);
        back.startAnimation(backInAnim);

        if (searchLay.getVisibility() == View.VISIBLE) {
            searchLay.setVisibility(View.GONE);
            searchLay.startAnimation(searchoutAnim);
        }

        if (myLocationFab.getVisibility() == View.VISIBLE) {
            myLocationFab.setVisibility(View.GONE);
            myLocationFab.startAnimation(fabOutAnim);
        }

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, (metrics.heightPixels) / 2, width / 10));
        time.setText(route.duration.text);
        distance.setText(route.distance.text);


        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.parseColor("#85AAC6")).
                width(10);

        for (int i = 0; i < route.points.size(); i++)
            polylineOptions.add(route.points.get(i));

        mMap.addPolyline(polylineOptions);

        final Marker car = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(MapsActivity.this, R.drawable.cab)))
                .anchor(0.5f, 0.5f)
                .position(latLngStart));
        animateMarker(route.points, car);
    }

    @Override
    public void onDirectionNotFound() {
        Toast.makeText(this, "No direction found", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );

        }
        currentLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        if (currentLocation != null) {
            Log.i("currentLocation", "" + currentLocation.getLatitude());
            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
            myLocation = new LatLng(latitude, longitude);
            latLngStart = myLocation;


            if (marker != null)
                marker.remove();
            if (markerStart != null)
                markerStart.remove();

            marker = mMap.addMarker(new MarkerOptions().position(myLocation));
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.my_marker_icon)));
            marker.setAnchor(0.5f, 0.5f);
            try {
                String addressString = getCompleteAddressString(latitude, longitude);
                starting.setText(addressString);
                markerStart = mMap.addMarker(new MarkerOptions().position(myLocation));
                markerStart.setPosition(latLngStart);
                markerStart.setAnchor(0.2f, 1f);
                markerStart.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.start_location, addressString.split(",")[0])));
                startAddress = addressString.split(",")[0];

            } catch (Exception e) {
                e.printStackTrace();
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLngStart)
                    .zoom(17).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));



        } else {
            Toast.makeText(this, "Unnable to get Your location", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Unnable to get Your location", Toast.LENGTH_SHORT).show();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(", ");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }


    public void showMe() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!gps_enabled && !network_enabled) {
            displayLocationSettingsRequest();
            // notify user
            /*AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("Enable Location in High accuracy mode");
            dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                    displayLocationSettingsRequest();
                }
            });
            dialog.show();*/
        } else {
            buildGoogleApiClient();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        }
    }

    private void displayLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MapsActivity.this, 3);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void showMe(View view) {
        showMe();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    public void cabType(View view) {
        ViewGroup viewGroup = (ViewGroup) view;
        ImageView imageView = (ImageView) viewGroup.getChildAt(0);
        imageView.setBackgroundResource(R.drawable.disk);
        imageView.setColorFilter(Color.WHITE);
        if (cabImagePrev != null && cabImagePrev != imageView) {
            cabImagePrev.setBackgroundColor(Color.WHITE);
            cabImagePrev.setColorFilter(Color.parseColor("#807d7d"));
        }
        cabImagePrev = imageView;
        cabImagePrev.startAnimation(carPick);
        switch (view.getId()) {
            case R.id.taxi:
                infoCab.setText("Some info about Taxi");
                break;
            case R.id.van:
                infoCab.setText("Some info about van");
                break;
            case R.id.disabilityVan:
                infoCab.setText("Some info about Disability Van");
                break;
            case R.id.groupVan:
                infoCab.setText("Some info about Group Touring Van");
                break;
            case R.id.shettle:
                infoCab.setText("Some info about Airport Shettle");
                break;
        }
    }

    public void rideNow(View view) {
        Intent i = new Intent(this, ConformationScreen.class);
        i.putExtra("distance", distance.getText().toString());
        i.putExtra("time", time.getText().toString());
        i.putExtra("startLatitude", latLngStart.latitude);
        i.putExtra("startLongitude", latLngStart.longitude);
        i.putExtra("startAddress", startAddress);
        i.putExtra("endLatitude", latLngEnd.latitude);
        i.putExtra("endLongitude", latLngEnd.longitude);
        i.putExtra("endAdress", endAdress);
        i.putExtra("schedule", false);
        startActivity(i);
    }

    public void scheduleRide(View view) {
        Intent i = new Intent(this, ConformationScreen.class);
        i.putExtra("distance", distance.getText().toString());
        i.putExtra("time", time.getText().toString());
        i.putExtra("startLatitude", latLngStart.latitude);
        i.putExtra("startLongitude", latLngStart.longitude);
        i.putExtra("startAddress", startAddress);
        i.putExtra("endLatitude", latLngEnd.latitude);
        i.putExtra("endLongitude", latLngEnd.longitude);
        i.putExtra("endAdress", endAdress);
        i.putExtra("schedule", true);
        startActivity(i);
    }

    /*private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }*/

    private void selectItem(int position) {
        switch (position) {
            case 0:
                Intent i = new Intent(this,ProfileActivity.class);
                startActivity(i);
                break;
            case 1:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 4:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 5:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 6:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            case 7:
                AppController.cleardatabase();
                Intent intent=new Intent(this,Intro.class);
                startActivity(intent);
                finish();
//                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "Under development", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
