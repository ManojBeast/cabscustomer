package com.cabscustomer.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cabscustomer.R;
import com.cabscustomer.mapModules.DirectionFinder;
import com.cabscustomer.mapModules.DirectionFinderListener;
import com.cabscustomer.mapModules.Route;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ConformationScreen extends AppCompatActivity implements OnMapReadyCallback, DirectionFinderListener {

    TextView distance, time;
    LatLng latLngStart, latLngEnd;
    GoogleMap mMap;
    CardView schedule;
    TextView scheduleDate, scheduleTime;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conformation_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        i = getIntent();

        distance = (TextView) findViewById(R.id.distance);
        time = (TextView) findViewById(R.id.time);
        schedule = (CardView) findViewById(R.id.schedule);

        scheduleDate = (TextView) findViewById(R.id.scheduleDate);
        scheduleTime = (TextView) findViewById(R.id.scheduleTime);


        distance.setText(i.getStringExtra("distance"));
        time.setText(i.getStringExtra("time"));
        latLngStart = new LatLng(i.getDoubleExtra("startLatitude", 0), i.getDoubleExtra("startLongitude", 0));
        latLngEnd = new LatLng(i.getDoubleExtra("endLatitude", 0), i.getDoubleExtra("endLongitude", 0));
        if (i.getBooleanExtra("schedule", false)) {
            schedule.setVisibility(View.VISIBLE);

            final Calendar calendar = Calendar.getInstance();
            final Date currentDateTime = Calendar.getInstance().getTime();

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy,hh:mm aa");
            String[] currentDateandTime = sdf.format(new Date()).split(",");
            scheduleDate.setText(currentDateandTime[0]);
            scheduleTime.setText(currentDateandTime[1]);


            scheduleDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String[] date = scheduleDate.getText().toString().split("/");

                    DatePickerDialog datePickerDialog = new DatePickerDialog(ConformationScreen.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            scheduleDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        }
                    }, Integer.parseInt(date[2]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[0]));
                    long now = System.currentTimeMillis();
                    try {
                        datePickerDialog.getDatePicker().setMinDate(now);
                        datePickerDialog.getDatePicker().setMaxDate(now + (1000 * 60 * 60 * 24 * 5));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    datePickerDialog.show();
                }
            });

            scheduleTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String[] time = scheduleTime.getText().toString().split(":");

                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(ConformationScreen.this, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                            scheduleTime.setText(((selectedHour < 12) ? selectedHour : selectedHour - 12) + ":" + selectedMinute + " " + ((selectedHour < 12) ? "AM" : "PM"));
                        }

                    }, (time[1].split(" ")[1].equals("AM")) ? Integer.parseInt(time[0]) : Integer.parseInt(time[0]) + 12, Integer.parseInt(time[1].split(" ")[0]), false);
                    mTimePicker.show();
                }
            });


        } else {
            schedule.setVisibility(View.GONE);

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        //mMap.getUiSettings().setRotateGesturesEnabled(false);
        //mMap.getUiSettings().setTiltGesturesEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        try {
            new DirectionFinder(this, latLngStart, latLngEnd).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });
    }


    @Override
    public void onDirectionFinderStart() {

    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes, String data) {
        Log.i("conformation route", data);
        if (routes.isEmpty()) {
            Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
            return;
        }

        Route route = routes.get(0);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(route.startLocation);
        builder.include(route.endLocation);
        Marker markerStart = mMap.addMarker(new MarkerOptions().position(latLngStart));
        markerStart.setAnchor(0.2f, 1f);
        markerStart.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.start_location, i.getStringExtra("startAddress"))));

        Marker markerEnd = mMap.addMarker(new MarkerOptions().position(latLngEnd));
        markerEnd.setAnchor(0.2f, 1f);
        markerEnd.setIcon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(this, R.drawable.end_location, i.getStringExtra("endAdress"))));

        Log.i("route.startAddress", route.startAddress);
        Log.i("route.endAddress", route.endAddress);
        //DisplayMetrics metrics = new DisplayMetrics();
        //getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //int width = metrics.widthPixels;
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));


        PolylineOptions polylineOptions = new PolylineOptions().
                geodesic(true).
                color(Color.parseColor("#85AAC6")).
                width(10);

        for (int i = 0; i < route.points.size(); i++)
            polylineOptions.add(route.points.get(i));

        mMap.addPolyline(polylineOptions);

    }

    public Bitmap getBitmapFromVectorDrawable(Context context, int drawableId, String title) {
        LinearLayout tv = (LinearLayout) getLayoutInflater().inflate(R.layout.marker_dialog, null, false);
        tv.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        tv.layout(0, 0, tv.getMeasuredWidth(), tv.getMeasuredHeight());
        ImageView markerIcon = (ImageView) tv.findViewById(R.id.markerIcon);
        tv.findViewById(R.id.next).setVisibility(View.GONE);
        TextView titleTv = (TextView) tv.findViewById(R.id.title);
        titleTv.setText(title);
        markerIcon.setImageResource(drawableId);
        tv.setDrawingCacheEnabled(true);
        tv.buildDrawingCache();
        Bitmap bm = tv.getDrawingCache();
        return bm;
    }


    @Override
    public void onDirectionNotFound() {

    }

    public void requestRide(View view) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage("Driver may take upto 20 minutes");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int paramInt) {
                dialogInterface.cancel();
            }
        });
        dialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dialog.show();
    }
}
