package com.cabscustomer.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.cabscustomer.R;
import com.cabscustomer.extras.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.cabscustomer.extras.AppController.userDetailsSqlDB;
import static com.cabscustomer.extras.AppController.userDetailsdb;

public class MobileVerification extends AppCompatActivity {
    EditText mobile_number,otp;
    ProgressBar progressbar;
    TextView countdowntime;
    Button resend,sendotp;
    String name,familyName,email,mobileNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        mobile_number= (EditText) findViewById(R.id.mobile_number);
        otp= (EditText) findViewById(R.id.otp);
        progressbar= (ProgressBar) findViewById(R.id.progressbar);
        countdowntime= (TextView) findViewById(R.id.countdowntime);
        resend= (Button) findViewById(R.id.resend);
        sendotp= (Button) findViewById(R.id.sendotp);
        Intent intent=getIntent();
        mobileNumber=mobile_number.getText().toString();

        final String name=intent.getExtras().getString("name");
        final String familyName=intent.getExtras().getString("familName");
        final String email=intent.getExtras().getString("email");
        Log.i("name",name);
        Log.i("familyName",familyName);
        Log.i("email",email);

        otp.setVisibility(View.INVISIBLE);
        progressbar.setVisibility(View.INVISIBLE);
        countdowntime.setVisibility(View.INVISIBLE);
        resend.setVisibility(View.INVISIBLE);
        sendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String MOBILE_URL = AppController.baseUrl + "ryder/RyderServices/CustomerDetails.aspx?"+"fN="+name+"&LastName="+familyName+"&eml="+email+"&rMno="+mobileNumber+"&Password="+"&countrycode=+91";
                Log.i("MOBILE_URL", MOBILE_URL);
                JsonArrayRequest req = new JsonArrayRequest(Request.Method.GET, MOBILE_URL, null,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                AppController.stopProgres();
                                Log.e("Response:", response.toString());
                                try {
                                    if (response.getJSONObject(0).has("Result") && response.getJSONObject(0).getString("Result").equals("-1")) {
                                    
                                        return;
                                    }
                                    Toast.makeText(MobileVerification.this, "Login successful", Toast.LENGTH_SHORT).show();
                                    JSONObject userDetailsJson = response.getJSONObject(0);
                                    String FirstName = userDetailsJson.getString("FirstName");
                                    String LastName = userDetailsJson.getString("LastName");
                                    String Email = userDetailsJson.getString("Email");
                                    String MobileNumber = userDetailsJson.getString("MobileNumber");

                                    userDetailsSqlDB = userDetailsdb.getReadableDatabase();
                                    String query = "insert into user values(?,?,?,?)";

                                    SQLiteStatement statement = userDetailsSqlDB.compileStatement(query);
                                    statement.bindString(1, FirstName);
                                    statement.bindString(2, LastName);
                                    statement.bindString(3, Email);
                                    statement.bindString(4, MobileNumber);
                                    long rowId = statement.executeInsert();

                                    userDetailsSqlDB.close();

                                    Intent i = new Intent(MobileVerification.this, MapsActivity.class);
                                    startActivity(i);
                                    Intro.activity.finish();
                                    MobileVerification.this.finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AppController.stopProgres();
                        Toast.makeText(MobileVerification.this, "Login failed", Toast.LENGTH_SHORT).show();
                        Log.e("Error: ", error.getMessage());
                    }
                });
                AppController.getInstance().addToRequestQueue(req);

                otp.setVisibility(View.VISIBLE);
                progressbar.setVisibility(View.VISIBLE);
                countdowntime.setVisibility(View.VISIBLE);
                resend.setVisibility(View.VISIBLE);
            }
        });

        
    }
}
