package com.cabscustomer.adaptors;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cabscustomer.R;
import com.cabscustomer.activities.Register;

/**
 * Created by acer 2 on 9/5/2017.
 */

public class FlagsAdaptor extends RecyclerView.Adapter<FlagsAdaptor.CustomViewHolder> {
    private Activity mContext;
    int[] flagId = {R.drawable.india, R.drawable.australia, R.drawable.new_zealand};
    String[] countryName = {"India (+91)", "Australia (+61)", "New Zealand (+64)"};

    public FlagsAdaptor(Activity mContex) {
        this.mContext = mContex;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //mContext = parent.getContext();
        View itemView = LayoutInflater.
                from(mContext).
                inflate(R.layout.country_card, parent, false);

        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        holder.flagImage.setImageResource(flagId[position]);
        holder.countryName.setText(countryName[position]);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra("position", position);
                mContext.setResult(1, i);
                mContext.finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return flagId.length;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView flagImage;
        TextView countryName;
        LinearLayout card;

        public CustomViewHolder(View itemView) {
            super(itemView);
            flagImage = (ImageView) itemView.findViewById(R.id.flagImage);
            countryName = (TextView) itemView.findViewById(R.id.countryName);
            card = (LinearLayout) itemView.findViewById(R.id.card);
        }
    }
}
