package com.cabscustomer.interfaces;

/**
 * Created by acer 2 on 9/6/2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
